import java.util.Scanner;

public class Homework05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int minDigit = a;
        int b;
        while (a != -1) {
            while (a != 0) {
                b = a % 10;

                if (b < minDigit) {
                    minDigit = b;
                }
                a = a / 10;

            }
            a = scanner.nextInt();
        }
        System.out.println(minDigit);
    }
}
