
public class Main {

    public static void selectionSort(Human[] array) {

        for (int i = 0; i < array.length; i++) {
            int min = array[i].getWeight();
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j].getWeight() < min) {
                    min = array[j].getWeight();
                    minIndex = j;
                }
            }
            Human temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
    }

    public static void main(String[] args) {

        Human[] humans = new Human[10];

        humans[0] = new Human();
        humans[0].setName("Ира");
        humans[0].setWeight(60);

        humans[1] = new Human();
        humans[1].setName("Вера");
        humans[1].setWeight(56);

        humans[2] = new Human();
        humans[2].setName("Лида");
        humans[2].setWeight(64);

        humans[3] = new Human();
        humans[3].setName("Маша");
        humans[3].setWeight(50);

        humans[4] = new Human();
        humans[4].setName("Катя");
        humans[4].setWeight(52);

        humans[5] = new Human();
        humans[5].setName("Дима");
        humans[5].setWeight(90);

        humans[6] = new Human();
        humans[6].setName("Саша");
        humans[6].setWeight(75);

        humans[7] = new Human();
        humans[7].setName("Паша");
        humans[7].setWeight(85);

        humans[8] = new Human();
        humans[8].setName("Миша");
        humans[8].setWeight(84);

        humans[9] = new Human();
        humans[9].setName("Сергей");
        humans[9].setWeight(82);

        selectionSort(humans);
        for (int i = 0; i < humans.length; i++) {
            System.out.println(humans[i].getName() + " " + humans[i].getWeight());

        }

    }
}
