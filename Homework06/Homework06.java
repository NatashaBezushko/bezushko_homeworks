import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {
        int[] arr = new int[10];
        int[] arr2 = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        int index = getIndex(arr2, 15);
        System.out.println("Индекс числа 15 в массиве = " + index);
        shift(arr2);
    }

    public static int getIndex(int[] arr, int value)  {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value)
                return i;
        }
        return -1;
    }

    public static void shift(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] == 0) {
                for (int j = i+1; j < arr.length; j++) {
                if(arr[j]!=0) {
                    arr[i] = arr[j];
                    arr[j] = 0;
                    i++;
                }
                count++;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
        System.out.println("operation = "+ count);
    }
}

